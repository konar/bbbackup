import os
import shlex
import subprocess
import json
import requests
from requests.auth import HTTPBasicAuth

import credentials as creds


HERE = os.path.dirname(os.path.abspath(os.path.realpath(__file__)))
backup_dir = os.path.join(HERE, 'backup')

api = requests.get(
    "https://bitbucket.org/!api/1.0/users/{}".format(creds.team),
    auth=HTTPBasicAuth(creds.login, creds.passwd)
)

jsonapi = api.json()


def run(cmd, *, workdir):
    print(cmd)
    args = shlex.split(cmd)
    subprocess.run(args, cwd=workdir, check=True)  # 'check' means: exception on error

os.mkdir(backup_dir)
for each in range(len(jsonapi['repositories'])):
    repo = jsonapi['repositories'][each]['slug']
    print("\nCloning: " + repo)

    gitclone = 'git clone --mirror https://{login}:{passwd}@bitbucket.org/konar/{repo} {destdir}'
    run(gitclone.format(
        login = creds.login,
        passwd = creds.passwd,
        repo = repo,
        destdir = os.path.join(repo, '.git')
    ), workdir=backup_dir)

    repodir = os.path.join(backup_dir, repo)
    run('git config --bool core.bare false', workdir=repodir)
    run('git reset --hard', workdir=repodir)
