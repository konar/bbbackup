# Bitbucket auto backup script

This script allows to backup the whole team repositories (all branches) using Bitbucket API and Git.

## Requirements

- `git`
- Bitbucket account

Requires user to create `credentials.py` file in the same folder with the following variables defined:

```python
login = "userlogin"
passwd = "userpasswd"
team = "team_or_user_name_to_clone_all_repositories_from"
```
